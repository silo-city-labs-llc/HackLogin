<?php
	require('hacklogin.class.php');
	
	$login = new Hacklogin();
	
	//The login page
	$login->setLoginPage("https://login.yahoo.com/");
	
	//The username and password in the form.
	$login->setLoginCredentials(array('username'=>'username','passwd'=>'password'))
	
	//Parameters for report data dump
	//This requires that you have a custom report called "Cart" inside of https://web.analytics.yahoo.com/
	$params = array(
		"reportId"=> "BOOKMARK_00000", "bookmarkReportId"=> "CUSTOM",
	    "reportName"=> "Cart", "createdBy"=> "something@yahoo.com",
		"reportDefinitionDetails"=> array(
	        "metrics"=>array(
	        	array( "id"=> 183 ),
	            array( "id"=> 181, "selectedAction"=> 2 ),
	            array( "id"=> 181, "selectedAction"=> 202 ),
	            array( "id"=> 209 )
	        ),
	        "leftDimensions"=> array(array( "id"=> 330 )),
	        "topDimensions"=>array()
	    ),
		"reportAttributes"=> array(
			"startDate"=> 2015-10-01,
	        "endDate"=> 2015-10-30,
	        "limit"=> 10,
	        "sort"=> array(
	            "column"=> 1,
	            "order"=> "desc"
	        )
	    ),
	    "chart"=> array(
	        "type"=> "BAR_VERTICAL",
	        "columns"=> array( 1 )
	    ),
	    "filterAttributes" => array( "segment"=> null ),
	    "pathReportAttributes" => new stdClass
	);
	
	//Make sure we have the security headers right
	$headers = array('X-YA-AUTH-MODE: YT','Content-Type: application/json');
	$url = 'https://repapi-us.web.analytics.yahoo.com/ya_api/v1/projects/1325700000/reportdata?noredirect=1';
	
	$json_dump = $yahoo->getPage($url,json_encode($params),$headers,true);
	$json_dump = json_decode($json_dump['data'],true);

	//Get the url of the dump and decode the json
	$yahoo_d = $yahoo->getPage($json_dump['reportLocation']['location']);
	$yahoo_d = json_decode($yahoo_d['data'],true);
	
	//Use this however you feel
	print_r($yahoo_d)
