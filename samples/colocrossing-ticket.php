<?php
	require('hacklogin.class.php');
	
	$login = new Hacklogin();
	
	//The login page
	$login->setLoginPage("https://portal.colocrossing.com/auth/login");
	
	//The username and password in the form.
	$login->setLoginCredentials(array('username'=>'','password'=>''));
	
	//Lets make our "API" call to a page
	$params = array(
		'ticket_subject' => 'Sample API Ticket',
		'ticket_department' => '2',
		'priority' => '1',
		//'ticket_devices[]' => '',
		'reply_text' => 'I need some help',
		'accept_tos' => '1',
	);
	
	$url = 'https://portal.colocrossing.com/support/addticket';
	$json_dump = $login->getPage($url,$params,null,true);

	//Now we can use this data however we want.
	echo $json_dump['http_code'].'<br>';
	echo $json_dump['data'];
