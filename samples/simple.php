<?php
	require('hacklogin.class.php');
	
	$login = new Hacklogin();
	
	//The login page
	$login->setLoginPage("https://www.domain.com/login/");
	
	//The username and password in the form.
	$login->setLoginCredentials(array('username'=>'username','passwd'=>'password'))
	
	//Lets make our "API" call to a page
	$params = array( "test1"=> "test", "test2"=> "test" );
	$url = 'https://www.domain.com/myaccount/';
	$json_dump = $login->getPage($url,$params,$headers,true);

	//Now we can use this data however we want.
	echo $json_dump['data'];
