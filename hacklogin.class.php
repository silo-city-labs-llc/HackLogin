<?php
    /**
     * @copyright Techreanimate 2015
	 * https://github.com/ldrrp/HackLogin
	 * @license MIT http://opensource.org/licenses/MIT
     */
    class HackLogin{
    	public $timeout = 10;
		
		private $login_page = '';
		private $login_action = '';
		private $login_params = array();
		
		public $cookie = '';
		public $cookiejar = '';
		public $agent = 'Mozilla/5.0 TRA-Login/1.0.0';
		
		public function __construct(){
			$this->cookiejar = tempnam('/tmp','cookie');
		}
		
		/**
		 * Sets the login page, This function also gets the exec and any hidden inputs
		 */
		public function setLoginPage($page){
			$this->login_page = $page;
			
			$ch = curl_init();
		    curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
		    curl_setopt($ch, CURLOPT_URL, $this->login_page);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiejar);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
		    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,5);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		
		    $login_page = curl_exec($ch);
		    curl_close($ch);
			
			//Set all the inputs
			if(preg_match_all("/<\s* input [^>]+ >/xi", $login_page, $array_pd)){
				foreach ($array_pd[0] as $key => $input) {
					preg_match('/name=["|\']([^"|^\']*)/',$input,$name);
					preg_match('/value=["|\']([^"|^\']*)/',$input,$value);
					$this->login_params[$name[1]] = isset($value[1]) ? $value[1]:'';
				}
			}
			
			//Get the action page
			preg_match_all('/action=["|\']([^"|^\']*)/', $login_page, $action);
			
			//Absolute or relative path?
			if(substr($action[1][0], 0, 1) == '/'){
				$url = parse_url($this->login_page);
				$this->login_action = $url['scheme'].'://'.$url['host'].$action[1][0];
			}else{
				$this->login_action = $action[1][0];
			}
		}
		
        /**
         * Login with the credentials
         * @param $credentials Array
		 * @return BOOLEAN
         */
        public function setLoginCredentials($credentials = array()){
        	//TODO: add check if login inputs exists prior to inserting them
        	foreach ($credentials as $key => $value) $this->login_params[$key] = $value;
        	$params = http_build_query($this->login_params);
	
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
			curl_setopt($ch, CURLOPT_URL, $this->login_action);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		    curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiejar);
		    curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiejar);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			$login_page = curl_exec($ch);
			$header = substr($login_page, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
			curl_close($ch);
			
			//Convert cookie to string
			$matches = preg_grep("/^Set-Cookie:\s*([^;]+)/", explode("\n", $header));
			
			$this->cookie = '';
			foreach ($matches as $key => $ck) {
				$ckg = explode(';',$ck);
				$this->cookie .= substr($ckg[0],12).';';
			}
			
			//Use cookie string or jar?
			if(!empty($this->cookie)){
				//Delete cookie jar
				unlink($this->cookiejar);
				$this->cookiejar = '';
			}
        }
		
		/**
		 * This sets the url as well as resets params and headers for a new call
		 */
		public function getPage($url = 'https://www.google.com', $params = '', $headers = '', $post = false){
			if (empty($url)) return false;
			
			if (!$post && !empty($params) && is_array($params)) {
				$url = $url . "?" . http_build_query($params);
			}
			
			$ch = curl_init($url);
			if ($post) {
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			}
			
			if (!empty($headers)) {
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			}
			
			curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
			
			if(file_exists($this->cookiejar)){
				curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiejar);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiejar);
			}else{
				curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
			}
			
			//curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
			$data['data'] = curl_exec($ch);
			$data['http_code'] = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
			curl_close($ch);
			return $data;
		}
    }
